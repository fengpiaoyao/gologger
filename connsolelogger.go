package gologger

import (
	"fmt"
)

type ConsoleLogger struct {
	Level LogLevel
}

//构造函数
func NewConsoleLogger(levelStr string) *ConsoleLogger {
	level, err := paseLogLevel(levelStr)
	if err != nil {
		panic(err)
	}
	return &ConsoleLogger{
		Level: level,
	}
}

//日志开关
func (c *ConsoleLogger) enable(lv LogLevel) bool {
	return lv >= c.Level
}

//格式化输出
func (c *ConsoleLogger) logFotmatOutput(lv LogLevel, fotmat string, a ...interface{}) {
	if c.enable(lv) {
		msg := fmt.Sprintf(fotmat, a...)
		funcName, fileName, lineNo, timestamp := getInfo(3)
		fmt.Printf("%s [%s] [file: %s/%s line: %d] %s \n", timestamp, getLogString(lv), funcName, fileName, lineNo, msg)
	}
}

func (c *ConsoleLogger) DEBUG(fotmat string, a ...interface{}) {
	c.logFotmatOutput(DEBUG, fotmat, a...)
}
func (c *ConsoleLogger) INFO(fotmat string, a ...interface{}) {
	c.logFotmatOutput(INFO, fotmat, a...)
}
func (c *ConsoleLogger) WARNING(fotmat string, a ...interface{}) {
	c.logFotmatOutput(WARNING, fotmat, a...)
}
func (c *ConsoleLogger) ERROR(fotmat string, a ...interface{}) {
	c.logFotmatOutput(ERROR, fotmat, a...)
}
func (c *ConsoleLogger) FATAL(fotmat string, a ...interface{}) {
	c.logFotmatOutput(FATAL, fotmat, a...)
}
