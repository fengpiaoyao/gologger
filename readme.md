


# 1 log级别
DEBUG
INFO
WARNING
ERROR
FATAL

# 2 输出方式
file  文件形式，默认100m自动切割，本地存储
console 控制台直接输出

# 3 quick start

引入：go get gitee.com/fengpiaoyao/gologger


	package main

	import (
		"fmt"
		"os"
		"time"
		"gitee.com/fengpiaoyao/gologger"
		"gopkg.in/ini.v1"
	)

	func main() {
		cfg, err := ini.Load("my.ini")
		if err != nil {
			fmt.Printf("Fail to read file: %v", err)
			os.Exit(1)
		}
		logLv := cfg.Section("log").Key("level").String()
		outPutType := cfg.Section("log").Key("type").String()

		mylog := gologger.FotmatLog(logLv, outPutType, cfg.Section("log").Key("path").String())

		for {
			_, err := os.Open("d:/mytest.txt")
			if err != nil {
				mylog.ERROR("错误信息", err)
			}
			mylog.DEBUG("debug日志")
			mylog.INFO("info日志")
			mylog.WARNING("WARNING")
			name := "什么啊"
			mylog.ERROR("ERROR,id %s", name)
			mylog.FATAL("FATAL")
			time.Sleep(time.Second)
		}
	}
